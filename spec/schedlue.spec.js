const schedlue = require('../src/schedlue')

describe('schedlue' , ()=> {


    it('"6:30", "30" ', () => {
        const expected = false
        const actual = schedlue("6:30", "30")
        expect(actual).toEqual(expected);
    })

    it('"8:30", "30" ', () => {
        const expected = true
        const actual = schedlue("8:30", "30")
        expect(actual).toEqual(expected);
    })

    it('"17:00", "30" ', () => {
        const expected = true
        const actual = schedlue("17:00", "30")
        expect(actual).toEqual(expected);
    })

    
    it('"9:30", "30" ', () => {
        const expected = true
        const actual = schedlue("9:30", "30")
        expect(actual).toEqual(expected);
    })

    it('"18:30", "30" ', () => {
        const expected = false
        const actual = schedlue("18:30", "30")
        expect(actual).toEqual(expected);
    })


    it('"8:20", "30" ', () => {
        const expected = false
        const actual = schedlue("18:30", "30")
        expect(actual).toEqual(expected);
    })

    xit(' should throw for empty string ', () => {
        expect(schedlue("", "")).toThrow("invalid params");
    })
})