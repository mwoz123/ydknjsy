
function randMax(max) {
    return Math.trunc(1E9 * Math.random()) % max;
}

var reel = {
    symbols: [
        'pik', 'hearts' , "rąb" , "trefl" , ":)", "star", "księżyc" , "słońce"
    ],
    spin() {
        if(this.position == null) {
            this.positon = randMax(this.symbols.length -1)
        }
        this.position = ( this.position = +100 + randMax(100)) % this.symbols.length; 
    },
    display() {
        if (this.position == null) {
            this.position = randMax(this.symbols.length -1)
        }
        return this.symbols[this.position]
    }
}

var reel3 = reel;
reel3.display =  function() {
        if (this.position == null) {
            this.position = randMax(this.symbols.length -1)
        }
        const before = this.position == 0 ? this.symbols.length -1 : this.position -1;
        const after = this.position == this.symbols.length -1 ? 0 : this.position +1;
        return [this.symbols[before], this.symbols[this.position], this.symbols[after]];
    };
var slotMachine = {
    reels: [
        // /this slot matchine needs 3 separte reels
        // hint : Object.create()
        Object.create(reel3), Object.create(reel3),Object.create(reel3)

    ],
    spin() {
        this.reels.forEach(element => {
            element.spin();
        });
    }
    ,
    display(){ 
        this.reels.forEach(element => {
            console.log(element.display());
        });
    }
}

slotMachine.spin();
slotMachine.display();