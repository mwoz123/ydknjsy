
const timeMin = parseStringTime('8:30')
const timeMax = parseStringTime('17:30')


function schedlue (timeStr, lengthStr) {
    if (!timeStr || ! lengthStr) throw new Error("invalid params")
    
    const currentStart = parseStringTime(timeStr)
    if ( currentStart >= timeMin ) {
        const length = parseInt(lengthStr);
        const currentEnd = new Date(currentStart.getTime())
        currentEnd.setMinutes(currentStart.getMinutes() + length);
        if (currentEnd <= timeMax) {
            return true
        }
    }
    return false;
}


function parseStringTime(timeStr) {
    const timeArr = timeStr.split(':');
    const hour = parseInt(timeArr[0]);
    const min = parseInt(timeArr[1])
    const date = new Date();
    date.setHours(hour)
    date.setMinutes(min, 0 ,0)
    return date;
}

module.exports = schedlue;